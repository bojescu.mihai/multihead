import { Net as networkDecorator } from "../../Common";

type Net = networkDecorator;

interface config {
    port: number,
    secret: string
}

interface connection {
    client: Net,
    capacities: string[],
    tasks: number[]
}

interface taskMap {
    counter: number,
    [task: number]: commandCallback
}

interface command extends Record<string, unknown> {
    command: string,
    payload: Record<string, unknown>
}

type commandCallback = (err: Error | null, data: Record<string, unknown> | void) => void

interface clientMessage extends Record<string, unknown> {
    type: string,
    err: string,
    data: Record<string, unknown>
}

export {
    Net,
    config,
    connection,
    taskMap,
    command,
    commandCallback,
    clientMessage
};
