import net from "net";

import { Common, Constants } from "../Utils";
import { IServer } from "../Types";

import { Net, Constants as NetConstants } from "../../Common";
import { commandCallback } from "../Types/Server";

class Server {
    config: IServer.config;
    connections: IServer.connection[];
    tasks: IServer.taskMap;

    constructor (config: IServer.config) {
        this.config = config;
        this.connections = [];
        this.tasks = {
            counter: 0
        };

        this.startServer();
    }

    act (command: IServer.command, callback: IServer.commandCallback): void {
        const commandCapacityIsString = typeof command.command === "string";
        const commandPayloadIsString = typeof command.payload === "object";
        const connectionsThatCanAnswer = this.connections.filter(connection => connection.capacities.find(capacity => capacity === command.command));
        const commandIsKnown = connectionsThatCanAnswer.length !== 0;

        if (!commandCapacityIsString || !commandPayloadIsString) {
            callback(new Error(Constants.errors.invalid.command));
        } else if (!commandIsKnown) {
            callback(new Error(Constants.errors.invalid.command));
        } else {
            doAct(this);
        }

        function doAct (self: Server) {
            const connection = connectionsThatCanAnswer[Math.random() * connectionsThatCanAnswer.length | 0];
            const counter = self.tasks.counter++;

            self.tasks[counter] = callback;
            connection.tasks.push(counter);

            const clientMessage: IServer.clientMessage = {
                type: NetConstants.types.task,
                err: "",
                data: {
                    ...command,
                    taskId: counter
                }
            };

            connection.client.send(clientMessage, NetConstants.defaults.length, err => {
                if (err) {
                    delete self.tasks[counter];
                    callback(err);
                }
            });
        }
    }

    private startServer () {
        const server = new net.Server();

        server.on("connection", this.onConnect(this));
        server.on("error", this.onServerError);

        server.listen(this.config.port);
    }

    private onConnect (self: Server) {
        return (socket: net.Socket) => {
            const client = new Net(null, socket);

            client.receiver.on(NetConstants.events.message, self.onMessage(client));
            client.receiver.on(NetConstants.events.disconnect, self.onDisconnect(client));
            client.receiver.on(NetConstants.events.error, self.onClientError(client));
        };
    }

    private onMessage (client: IServer.Net) {
        return (message: IServer.clientMessage) => {
            switch (message.type) {
                case NetConstants.types.init:
                    return handleInit(this, client, message);
                case NetConstants.types.capacities:
                    return handleCapacities(this, client, message);
                case NetConstants.types.task:
                    return handleTask(this, client, message);
                default:
                    return this.onClientError(client)(new Error(Constants.errors.invalid.type));
            }
        };

        function handleInit (self: Server, client: IServer.Net, message: IServer.clientMessage) {
            const secretOk = message.data?.secret === self.config.secret;

            if (!secretOk) {
                self.onClientError(client)(new Error(Constants.errors.invalid.credentials));
            } else {
                doAct();
            }

            function doAct () {
                const messageForClient = {
                    type: NetConstants.types.init,
                    err: "",
                    data: {
                        message: "ok"
                    }
                };

                client.send(messageForClient, NetConstants.defaults.length, err => {
                    if (err) {
                        self.onClientError(client)(err);
                    } else {
                        self.connections.push({
                            client,
                            capacities: [],
                            tasks: []
                        });
                    }
                });
            }
        }

        function handleCapacities (self: Server, client: IServer.Net, message: IServer.clientMessage) {
            const capacitiesIsArray = Array.isArray(message.data?.capacities);
            const capacitiesAreStrings = capacitiesIsArray && !(message.data?.capacities as Array<unknown>).find((item: unknown) => typeof item !== "string");
            const clientIndex = Common.findClient(self.connections, client);
            const clientIsKnown = clientIndex !== -1;

            if (!clientIsKnown) {
                self.onClientError(client)(new Error(Constants.errors.invalid.credentials));
            } else if (!capacitiesAreStrings) {
                self.onClientError(client)(new Error(Constants.errors.format.capacities));
            } else {
                doAct();
            }

            function doAct () {
                self.connections[clientIndex].capacities = message.data?.capacities as Array<string>;

                const messageForClient = {
                    type: NetConstants.types.capacities,
                    err: "",
                    data: {
                        message: "ok"
                    }
                };

                client.send(messageForClient, NetConstants.defaults.length, err => {
                    if (err) {
                        self.onClientError(client)(err);
                    } else {
                        self.connections[clientIndex].capacities = message.data?.capacities as Array<string>;
                    }
                });
            }
        }

        function handleTask (self: Server, client: IServer.Net, message: IServer.clientMessage) {
            const clientIndex = Common.findClient(self.connections, client);
            const clientIsKnown = clientIndex !== -1;
            const messageTask = message.data?.taskId;
            const messageHasTaskId = messageTask !== undefined;
            const messageTaskIdIsNumber = messageHasTaskId && typeof messageTask === "number";
            const task = messageTaskIdIsNumber && self.tasks[messageTask as number];
            const error = message.err;
            const data = message.data;

            if (!clientIsKnown) {
                self.onClientError(client)(new Error(Constants.errors.invalid.credentials));
            } else if (!messageHasTaskId || !messageTaskIdIsNumber || !task) {
                self.onClientError(client)(new Error(Constants.errors.invalid.task));
            } else {
                doAct();
            }

            function doAct () {
                const connectionTasks = self.connections[clientIndex].tasks;

                delete self.tasks[messageTask as number];
                connectionTasks.splice(connectionTasks.indexOf(messageTask as number), 1);

                if (error) {
                    (task as commandCallback)(new Error(error));
                } else {
                    (task as commandCallback)(null, data);
                }
            }
        }
    }

    private onDisconnect (client: IServer.Net) {
        return () => {
            if (client) {
                const clientIndex = Common.findClient(this.connections, client);
                const clientIsKnown = clientIndex !== -1;

                if (clientIsKnown) {
                    this.connections[clientIndex].tasks.forEach(task => {
                        this.tasks[task](new Error(NetConstants.errors.client.disconnected));
                        delete this.tasks[task];
                    });
                    this.connections.splice(clientIndex, 1);
                }

                client.close();
            }
        };
    }

    private onServerError (error: Error) {
        console.error("[Multihead - server error]", error.stack);
    }

    private onClientError (client: IServer.Net) {
        return (error: Error) => {
            const clientIndex = Common.findClient(this.connections, client);
            const clientIsKnown = clientIndex !== -1;

            if (clientIsKnown) {
                this.connections[clientIndex].tasks.forEach(task => {
                    this.tasks[task](new Error(NetConstants.errors.client.disconnected));
                    delete this.tasks[task];
                });

                this.connections.splice(clientIndex, 1);
            }

            const messageForClient = {
                type: NetConstants.types.error,
                err: error.message
            };

            client.send(messageForClient, NetConstants.defaults.length, () => {
                client.close();
            });

            console.error("[Multihead - client error]", error.stack);
        };
    }
}

export default Server;
