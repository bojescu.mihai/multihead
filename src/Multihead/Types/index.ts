import * as multihead from "./Multihead";
import * as utils from "./Utils";

export {
    multihead as IMultihead,
    utils as IUtils
};
